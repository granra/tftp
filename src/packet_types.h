struct packet_in {
	short opcode;
	char buf[514];
};

struct packet_data {
	short opcode;
	short block;
	char data[512];
};

struct packet_ack {
	short opcode;
	short block;
};

struct packet_err {
	short opcode;
	short errcode;
	char msg[512];
};
