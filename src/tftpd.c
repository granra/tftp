#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <time.h>
#include "packet_types.h"

#define BUFLEN sizeof(struct packet_in)
#define HEADLEN 2 * sizeof(short)

/* opcodes */
#define OP_RRQ  1
#define OP_WRQ  2
#define OP_DATA 3
#define OP_ACK  4
#define OP_ERR  5

/* Stores the full path to the directory
 * containing the files to serve */
char base_path[100];

int verbose = 0;

void error(char* error) {
	printf("Error: %s\n", error);
	exit(1);
}

void senderror(int sockfd, struct sockaddr* client, int errcode, char* msg) {
	struct packet_err err;
	socklen_t clen = (socklen_t) sizeof(*client);
	err.opcode = htons(OP_ERR);
	err.errcode = htons(errcode);
	strcpy(err.msg, msg);
	sendto(sockfd, &err, HEADLEN + strlen(err.msg) + 1, 0, client, clen);
}

int readdata(struct packet_data* data, int block, FILE* file) {
	data->opcode = htons(OP_DATA);
	data->block  = htons(block);
	return fread(data->data, sizeof(char), 512, file);
}

FILE *openfile(char *buf) {
	/* 100 bytes of base_path + 255 byte filename = 355 */
	char *mode,
	     *fullpath = malloc(355),
		 *tmp      = malloc(355);
	FILE* f;
	/* Find requested mode */
	int i = strlen(buf);
	mode = &buf[++i];

	/* Concatinate file to base path */
	if (snprintf(tmp, 355, "%s/%s", base_path, buf) > 354) {
		/* snprintf truncated filename */
		free(fullpath);
		free(tmp);
		return NULL;
	}

	/* Evaluate path */
	realpath(tmp, fullpath);
	free(tmp);

	/* Check if requested file is out of base path */
	if (strncmp(fullpath, base_path, strlen(base_path))) {
		/* fullpath is outside of base_path
		 * e.g. client requested ../Makefile */
 		free(fullpath);
		return NULL;
	}

	if (verbose > 0) {
		printf("mode: %s\nfullpath: %s\n", mode, fullpath);
	}
	if (strcasecmp(mode, "octet")) {
		f = fopen(fullpath, "rb");
	}
	else {
		f = fopen(fullpath, "r");
	}

	free(fullpath);
	return f;
}

int initsocket(int port, int timeout) {
	struct sockaddr_in server;
	int sockfd;
	/* Create a UDP socket */
	if ((sockfd=socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
		return -1;
	}

	memset(&server, 0, sizeof(server));
	server.sin_family      = AF_INET;
	server.sin_port        = htons(port);
	server.sin_addr.s_addr = htonl(INADDR_ANY);

	if (timeout > 0) {
		/* Set timeout on socket */
		struct timeval tv;
		tv.tv_sec = timeout;
		tv.tv_usec = 0;
		setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (char*)&tv, sizeof(struct timeval));
	}

	/* Binding socket to port */
	if (bind(sockfd, (struct sockaddr*)&server, (socklen_t)sizeof(server)) == -1) {
		return -1;
	}

	return sockfd;
}

void sendfile(struct sockaddr_in client, char* buf) {
	socklen_t clen = (socklen_t) sizeof(client);
	char client_addr[20];
	int  sockfd, client_port = ntohs(client.sin_port), data_size;
	FILE *file;

	/* Get the address of the client */
	strcpy(client_addr, inet_ntoa(client.sin_addr));

	/* verbose */
	printf("file \"%s\" requested from %s:%d\n", buf, client_addr, client_port);

	/* Generate random port number */
	int server_port = (rand() % 64530) + 1000;

	if ((sockfd = initsocket(server_port, 5)) == -1) {
		/* Failed when creating a socket */
		return;
	}

	if ((file = openfile(buf)) == NULL) {
		senderror(sockfd, (struct sockaddr*)&client, 1, "File not found");
		close(sockfd);
		return;
	}

	/* Initial block number */
	int block = 1;
	int acked = 0;

	/* Initial packet structs */
	struct packet_in* response = malloc(sizeof(struct packet_in));
	struct packet_data* data = malloc(sizeof(struct packet_data));

	/* Read initial data block */
	data_size = readdata(data, block, file);

	int recv_len;

	do {
		/* Check if previous block was acked */
		if (acked) {
			data_size = readdata(data, block, file);
		}

		/* Send Data */
		if (verbose > 1) {
			printf("Sending opcode: %d, block: %d\n",
				ntohs(data->opcode), ntohs(data->block));
		}
		sendto(sockfd, data, HEADLEN + data_size, 0,
				(struct sockaddr*)&client, clen);
		acked = 0;

		/* Wait for some packets */
		if ((recv_len = recvfrom(sockfd, response, BUFLEN, 0, (struct sockaddr*)&client, &clen)) == -1) {
			/* Timeout expired */
			if (errno == EAGAIN) {
				break;
			}
		}

		if (verbose > 1) {
			printf("Received opcode: %d, block: %d\n", ntohs(response->opcode),
					ntohs(((struct packet_ack*)response)->block));
		}

		/* Check if packet is from the correct TID */
		if (ntohs(client.sin_port) != client_port) {
			if (verbose > 0) printf("Transfer id unknown sent\n");
			senderror(sockfd, (struct sockaddr*)&client, 5, "Transfer id unknown");
			continue;
		}

		/* Check if packet is ack for last sent data packet */
		if (ntohs(response->opcode) == OP_ACK
			&& ntohs(((struct packet_ack*)response)->block) == block) {
			block++;
			acked = 1;
		}
	} while (data_size > 511 || !acked);

	free(data);
	free(response);
	fclose(file);
	close(sockfd);
}

int main(int argc, char* argv[]) {
	struct sockaddr_in client;
	int sockfd;
	srand(time(NULL));

	/* Check if enough parameters are provided */
	if (argc < 3) error("Not enough parameters provided. Exiting...");

	/* Get full path of the directory */
	realpath(argv[2], base_path);

	/* Initialize socket */
	sockfd = initsocket(strtol(argv[1], NULL, 0), 0);

	/* Initialize packet structure */
	struct packet_in* request = malloc(sizeof(struct packet_in));

	/* Initialize variables used in the loop */
	int recv_len;
	socklen_t clen = (socklen_t) sizeof(client);

	while(1) {
		/* Wait for some packets */
		if ((recv_len = recvfrom(sockfd, request, BUFLEN, 0, (struct sockaddr*)&client, &clen)) == -1) {
			error("Could not receive packet");
		}

		/* Make sure the packet ends with a 0 byte */
		request->buf[recv_len - 2] = '\0';

		/* Endian conversion on opcode */
		request->opcode = ntohs(request->opcode);

		/* Check opcode */
		if (request->opcode == OP_RRQ) {
			sendfile(client, request->buf);
			if (verbose > 0) printf("Back to main loop\n");
		}
		else if (request->opcode == OP_WRQ) {
			senderror(sockfd, (struct sockaddr*)&client, 0, "Can't accept write request");
		}
		else {
			senderror(sockfd, (struct sockaddr*)&client, 4, "Illegal operation");
		}
	}

	/* Close server socket */
	close(sockfd);

	return 0;
}
